import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 8 X 8
 */
public class Board
{
    private final int width;
    private final int height;

    private final Set<Square> squares;
    private final Set<Square> unthreatened;

    private final Set<Square> queens;

    public Board(int x, int y)
    {
        this.width = x;
        this.height = y;
        this.queens = new LinkedHashSet<>();

        this.squares = generateSquares(x, y);
        this.unthreatened = squares;
    }

    public Board(final Board board, final Square newQueen)
    {
        this.width = board.width;
        this.height = board.height;
        this.squares = board.squares;

        final Set<Square> queens = new LinkedHashSet<>(board.queens);
        queens.add(newQueen);
        this.queens = queens;

        this.unthreatened = new LinkedHashSet<>(board.unthreatened);
        this.unthreatened.removeAll(getThreatened(newQueen));
    }


    public Board placeQueen(final Square newQueen)
    {
        return new Board(this, newQueen);
    }

    public Set<Square> getUnthreatened()
    {
        return unthreatened;
    }

    public Set<Square> getQueens()
    {
        return queens;
    }

    /**
     * If I place a queen here where is threatened.
     */
    private Set<Square> getThreatened(final Square location)
    {
        final Set<Square> threatened = new LinkedHashSet<>();
        for (int i = 0; i < Math.max(width, height); i++)
        {
            final int x = location.getX();
            final int y = location.getY();
            threatened.add(new Square(x + i, y));
            threatened.add(new Square(x, y + i));

            threatened.add(new Square(x - i, y));
            threatened.add(new Square(x, y  - i));

            threatened.add(new Square(x + i, y + i));
            threatened.add(new Square(x - i, y - i));
            threatened.add(new Square(x - i, y + i));
            threatened.add(new Square(x + i, y - i));
        }

        // Might not need to bother with this...
        return threatened.stream()
                  .filter(s -> s.getX() <= width)
                  .filter(s -> s.getX() > 0)
                  .filter(s -> s.getY() <= height)
                  .filter(s -> s.getY() > 0)
                  .collect(Collectors.toSet());
    }

    private Set<Square> generateSquares(final int width, final int height)
    {
        final Set<Square> threatened = new LinkedHashSet<>();
        for (int x = 1; x <= width; x++)
        {
            for (int y = 1; y <= height; y++)
            {
                threatened.add(new Square(x, y));
            }
        }
        return threatened;
    }

    @Override
    public String toString()
    {
        final StringBuilder out = new StringBuilder();
        for (int x = 1; x <= width; x++)
        {
            out.append("\n");
            for (int y = 1; y <= height; y++)
            {
                final Square location = new Square(x, y);
                if (queens.contains(location)) {
                    out.append(" Q");
                }
                else if (unthreatened.contains(location)) {
                    out.append(" X");
                }
                else {
                    out.append(" _");
                }
            }
        }
        return out.toString();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        Board board = (Board) o;
        return width == board.width && height == board.height && queens.equals(board.queens);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(width, height, queens);
    }
}
