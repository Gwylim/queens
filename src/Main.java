import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Main
{
    private static final int WIDTH = 8;

    public static void main(String[] args)
    {
        final Set<Board> boardsToExplore = new LinkedHashSet<>();
        boardsToExplore.add(new Board(WIDTH, WIDTH));

        final Set<Board> solutions = new LinkedHashSet<>();
        final Set<Board> explored = new LinkedHashSet<>();

        while(!boardsToExplore.isEmpty()) {
            System.out.println(".");

            final Set<Board> done = new LinkedHashSet<>(boardsToExplore);

            final Set<Board> newBoards
                    = boardsToExplore.stream()
                                     .peek(explored::add)
                                     .peek(done::add)
                                     .flatMap(
                            exploring -> exploring.getUnthreatened().stream()
                                                  .map(exploring::placeQueen)
                                    .filter(e -> !explored.contains(e))
                                                  ).collect(Collectors.toSet());

            boardsToExplore.removeAll(done);
            boardsToExplore.addAll(newBoards);

            newBoards.stream()
                    .filter(b -> b.getUnthreatened().isEmpty())
                    .filter(b -> b.getQueens().size() == WIDTH)
                    .forEach(e -> {
                        solutions.add(e);
                        boardsToExplore.remove(e);
                        System.out.println(e);
                    });
        }

        System.out.println("Solutions: " + solutions.size());
    }

}
